import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './complements/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { NavbarComponent } from './complements/navbar/navbar.component';
import { ListMenuComponent } from './complements/list-menu/list-menu.component';
import { BlankpageComponent } from './pages/blankpage/blankpage.component';
import { ModalQuickbuyComponent } from './complements/modals/modal-quickbuy/modal-quickbuy.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import {OverlayPanelModule} from 'primeng/overlaypanel';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {ButtonModule} from 'primeng/button';
import { FormValidatorsComponent } from './pages/form-validators/form-validators.component';
import { CreateAccountComponent } from './pages/create-account/create-account.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HomeComponent,
    NavbarComponent,
    ListMenuComponent,
    BlankpageComponent,
    ModalQuickbuyComponent,
    FormValidatorsComponent,
    CreateAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    OverlayPanelModule,
    ButtonModule,
    ReactiveFormsModule,
    ScrollPanelModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
