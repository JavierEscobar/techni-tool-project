import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlankpageComponent } from './pages/blankpage/blankpage.component';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { FormValidatorsComponent } from './pages/form-validators/form-validators.component';
import { HomeComponent } from './pages/home/home.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'white', component: BlankpageComponent },
  { path: 'form', component: FormValidatorsComponent },
  { path: 'create-account', component: CreateAccountComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
