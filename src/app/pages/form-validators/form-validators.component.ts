import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-validators',
  templateUrl: './form-validators.component.html',
  styleUrls: ['./form-validators.component.scss']
})
export class FormValidatorsComponent implements OnInit {

  public formulario: FormGroup;
  dropdown1: boolean;
  dropdown2: boolean;
  openEditPass: boolean;

  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.dropdown1 = false;
    this.dropdown2 = false;
    this.openEditPass = false;

    this.formulario = this.formBuilder.group({
      email: new FormControl({ value: '', disabled: true }, Validators.email),
      pasword: new FormControl({ value: '', disabled: true }),
      firstName: new FormControl({ value: '', disabled: true }),
      lastName: new FormControl({ value: '', disabled: true }),
      companyName: new FormControl({ value: '', disabled: true }),
      phone: new FormControl({ value: '', disabled: true }),
      ext: new FormControl({ value: '', disabled: true }),
    });
  }

  habilitarCampos(value: string) {
    if (!this.formulario.get(value).disabled) {
      this.formulario.get(value).disable();
    } else {
      this.formulario.get(value).enable();
    }
  }

}
