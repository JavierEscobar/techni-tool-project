import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  public formulario: FormGroup;
  toggle: boolean;
  showtooltip: boolean;


  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.toggle = false;
    this.showtooltip = false;
    this.formulario = this.formBuilder.group({
      billingaddress: new FormControl(''),
      accountnumber: new FormControl(''),
      email: new FormControl('', Validators.email),
      pasword: new FormControl(''),
      paswordconfirm: new FormControl(''),
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      companyName: new FormControl(''),
      phone: new FormControl(''),
    });

  }

}
