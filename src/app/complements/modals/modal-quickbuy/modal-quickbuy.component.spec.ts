import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalQuickbuyComponent } from './modal-quickbuy.component';

describe('ModalQuickbuyComponent', () => {
  let component: ModalQuickbuyComponent;
  let fixture: ComponentFixture<ModalQuickbuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalQuickbuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalQuickbuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
