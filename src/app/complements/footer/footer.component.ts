import { Component, OnInit, ElementRef, ViewChildren, Renderer2, ViewChild, QueryList } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @ViewChildren('artic') articles: QueryList<any>;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
  }

  scrollToElement(): void {
    const $element = document.querySelector("#destination")
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }

  toggleFooterSection(e) {
    const articlePrincipal = e.target.parentElement.parentElement.querySelector("article");
    const filteredList = this.articles.filter(x => x.nativeElement.id != articlePrincipal.id );
    filteredList.forEach(artic => this.renderer.addClass(artic.nativeElement, 'h-0'));
    if (articlePrincipal.classList.contains("h-0")) {
      articlePrincipal.classList.remove("h-0");
    } else {
      articlePrincipal.classList.add("h-0");
    }
  }

}
