import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from 'src/services/modal.service';
import { WindowService } from 'src/services/window.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  showDropdown: boolean;
  showDropdownMobile: boolean;
  showMobileMenu: boolean;
  searchValue: any;


  constructor(public _windowService: WindowService, public router: Router, public _modalService: ModalService) { }

  ngOnInit(): void {
    this.showDropdown = false;
    this.showMobileMenu = false;
    this.showDropdownMobile = false;
    this.searchValue = '';
    

  }

  minimenuDropdown() {
    this.showDropdownMobile = !this.showDropdownMobile;
    this.showMobileMenu = false;
  }

  mobileMenu() {
    this.showMobileMenu = !this.showMobileMenu;
    this.showDropdownMobile = false;
  }


  clearSearch() {
    this.searchValue = '';
  }

  showQuickbuyModal(){
    this.showDropdown = false;
    this.showMobileMenu = false;
    this.showDropdownMobile = false;
    this._modalService.setOpenQuickBuy(true)
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.showDropdown = false;
    this.showDropdownMobile = false;
    this.showMobileMenu = false;
  }

}
