import { Component, HostListener, OnInit } from '@angular/core';
import { ModalService } from 'src/services/modal.service';
import { WindowService } from 'src/services/window.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public innerWidth: any;

  constructor(private _windowService: WindowService, public _modalService: ModalService) { }

  ngOnInit(): void {
    this._windowService.setShowMenu(window.innerWidth > 1024 ? true : false);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this._windowService.setShowMenu(window.innerWidth > 1024 ? true : false);
  }
}
