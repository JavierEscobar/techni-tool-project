import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WindowService {

  private showMenu : boolean;
  constructor() { }

  setShowMenu(value: boolean){
    this.showMenu = value;
  }

  getShowMenu():boolean{
    return this.showMenu;
  }

}
