import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private openModal: boolean;
  private openQuickBuy: boolean;

  constructor() {
    this.resetModals();
  }

  public resetModals() {
    this.openModal = false;
    this.openQuickBuy = false;
  }

  public modalIsOpen(): boolean {
    return this.openModal;
  }

  public setModalIsOpen(value: boolean) {
    this.openModal = value;
  }

  public getOpenQuickBuy(): boolean {
    return this.openQuickBuy;
  }

  public setOpenQuickBuy(value: boolean) {
    this.openQuickBuy = value;
    this.openModal = value
  }



}
